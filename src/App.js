import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { connect } from 'react-redux';
import Signin from './Component/Container/Template/Login'
import ChatForm from './Component/Container/Template/Chat'
class App extends Component {

  render() {
    return (
      <BrowserRouter>
      <div className="App">
      <Switch>
      <Route exact path='/' component = {ChatForm} />
        <Route exact path='/login' component = {Signin} />
        <Route path='/dasboard' component = {ChatForm} />
        
      </Switch>
      </div>
      </BrowserRouter>
    );
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    checkLogin: () => {
      dispatch({type:"LOG_IN"})
    }
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
    isSignedin: state.auth.isSignedin
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
