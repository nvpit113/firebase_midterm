import React from 'react'
import { connect } from 'react-redux'
import { compose }  from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import OnePeopleOnList from './OnePeopleOnList'
class PeopleList extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            peopleSearchKeyword: ''
        };
        this.searchHandler = this.searchHandler.bind(this);
    }
    
    searchHandler (event) {
        let searchKey = event.target.value.toLowerCase();
        console.log(searchKey);
        
        this.setState({
            peopleSearchKeyword: searchKey
        })
      }
    render() {
        let contacts = this.state.displayedContacts;
        
        return (
            <div className="people-list" id="people-list">
                <div className="search">
                    <input type="text" placeholder="search" onChange={this.searchHandler}/>
                    <i className="fa fa-search" />
                </div>

                <ul className="list">
                    {this.props.people_list ? 
                    this.props.people_list.map((one_user, index) => {
                        let searchValue = one_user['displayName'].toLowerCase();
                        return (searchValue.indexOf(this.state.peopleSearchKeyword) !== -1 && one_user.uid != this.props.cur_uid) ? <OnePeopleOnList data={one_user} key={index}/>:'';
                    })
                    :
                    ''}
                
                </ul>
            </div>
        );
    }
}
export default compose(
    firestoreConnect(['users']), // or { collection: 'todos' }
    connect((state, props) => ({
      people_list: state.firestore.ordered.users,
      cur_uid: state.firebase.auth.uid
    }))
   )(PeopleList)