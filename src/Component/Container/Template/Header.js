import React from 'react';
import { connect } from 'react-redux'
import {Redirect } from 'react-router-dom'
import { signOut} from './../../Presentation/Store/Action/Auth'
class Header extends React.Component {
    
    render(){
    if (!this.props.userName) {
        return <Redirect to='/login'/>
    }
    else
    return(
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" href="#">FIREBASE CHAT</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon" />
                </button>
                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav ml-auto">
                    {/* <li className="nav-item active">
                        <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                    </li> */}
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {this.props.userName? this.props.userName: ''}
                            </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <button className="dropdown-item"  onClick={() =>this.props.signOut(this.props.uid)}>Sign out</button>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>

        </div>
    )
                }
}

const mapStateToProps = (state, ownProps) => {
    return {
        uid: state.firebase.auth.uid, 
        userName: state.firebase.auth.displayName,
        userPhoto: state.firebase.auth.photoURL
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        signOut: (uid) => {
            dispatch(signOut(uid))
        }
    }
}
export default connect(mapStateToProps,mapDispatchToProps) (Header)