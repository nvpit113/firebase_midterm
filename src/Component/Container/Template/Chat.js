import React from 'react'
import Header from './Header.js'
import './../Css/Chat.css'
import { connect } from 'react-redux'
import PeopleList from './PeopleList'
import MessageBox from './MessageBox'
class Chat extends React.Component { 
    
    render(){        
        return (
    <div>
        <Header></Header>

        <div className="chat-container clearfix">

            <PeopleList/>
            
            <div className="chat">
                
                <MessageBox chatID={this.props.chatID}/>

            </div> {/* end chat */}
            </div> {/* end container */}

    </div>
)
        }
}
const mapStateToProps = (state, ownProps) => {
    return {
        chatID: state.chat.chatID
    }
}

export default connect(mapStateToProps) (Chat)