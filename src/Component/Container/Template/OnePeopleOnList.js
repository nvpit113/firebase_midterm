import React from 'react'
import { connect } from 'react-redux'
import {startChat} from './../../Presentation/Store/Action/Chat'



var formatDate = (date1) => {
    var date = new Date(date1);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return date.getMonth()+1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
  }
  
class OnePeopleOnList extends React.Component {

    constructor(props) {
        super(props);
        
    }
    
    handleClickToChat = () =>{

        let id = this.props.data.uid;
        let display_name = this.props.data.displayName;
        let photo_url = this.props.data.photoURL;
        //Lấy id của lịch sử chat bằng cách ghép id của 2 user lại
        let cur_uid = this.props.cur_uid;
        let chat_id = '';
        if (id > cur_uid)
            chat_id = cur_uid.concat('_'+id);
        else
            chat_id = id.concat('_'+cur_uid);
        
        this.props.startChat(id, display_name, photo_url, chat_id);
    }
    render() {
    if (this.props.data.isOnline) {
        return(
            <li className="clearfix">
                <img src={this.props.data.photoURL} onError={(e)=>{e.target.onerror = null; e.target.src="https://via.placeholder.com/150"}}  alt={this.props.data.displayName} onClick = {()=>this.handleClickToChat()
                }/>
                <div className="about">
                <div className="name">{this.props.data.displayName}</div>
                <div className="status">
                    <i className="fa fa-circle online" /> online
                </div>
                </div>
            </li>
        )
    }
    else
        return(
            <li className="clearfix">
                <img src={this.props.data.photoURL} alt={this.props.data.displayName} onError={(e)=>{e.target.onerror = null; e.target.src="https://via.placeholder.com/150"}}  alt={this.props.data.displayName} onClick = {()=>this.handleClickToChat()}/>
                <div className="about">
                <div className="name">{this.props.data.displayName}</div>
                <div className="status">
                    <i className="fa fa-circle offline" /> {formatDate(this.props.data.lastSignIn)}
                </div>
                </div>
            </li>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        cur_uid: state.firebase.auth.uid
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        startChat: (id, display_name, photo_url, chat_id) => {
            dispatch(startChat(id, display_name, photo_url, chat_id))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(OnePeopleOnList)