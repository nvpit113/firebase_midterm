import React from 'react'
import { connect } from 'react-redux'
import { compose }  from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import {sendMessage } from './../../Presentation/Store/Action/Chat'
import { subscribe } from './../../Presentation/Store/Action/Auth'

var formatDate = (date1) => {
    var date = new Date(date1);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return date.getMonth()+1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
  }
class MessageBox extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            message: '',
            isSubscribed: false
        }
    }
    isActive = () =>{
        
        let isSub = (this.state.isSubscribed == true) ? 'subscribed':'no-subscribed';
        return 'fa fa-star '+ isSub;
    }
    handleSubscribe = () => {
        let myID = this.props.myID;
        let subscribes = this.props.subscribes;
        let sID = this.props.uid;

        let pos = subscribes.indexOf(sID);
        if (pos > -1) {
            subscribes.splice(pos, 1);
        }
        else{
            subscribes.push(sID);
        }

        this.props.subscribe(myID,subscribes);

        this.setState({
            isSubscribe: !this.state.isSubscribe
        })
    }
    handleMessage = (event) =>{
        this.setState({message: event.target.value});
    }

    handleSendMessage = () =>{
        let chat_id = this.props.chatID;
        let sender = this.props.myID;
        let message = this.state.message;
        
        if (message.length > 0) {
            this.props.sendMessage(chat_id, sender, message);
        }

        this.setState({
            message: ''
        })
    }

    convertWordToImage = (word) =>{
        let img_extendtion = ['.jpg', '.JPG', '..png', '.PNG'];
        let pos = -1;
        for (let i = 0; i < img_extendtion.length; i++) {
            const element = img_extendtion[i];
            if (word.search(element) > 0) {
                pos = word.search(element);
            }
        }
        if (pos > -1) {
            let img_link = word.slice(0, pos+4);
            return <img src= {img_link} key = {img_link} alt="avatar" />;
        }
        return word+" ";
    }

    componentWillMount(){
        
        let isSubscribed = false;
        if (this.props.allUsers) {
            this.props.allUsers.forEach(element => {
                if (element.uid == this.props.myID) {
                    isSubscribed = true;
                }
            });
        }
        this.setState({
            isSubscribed: isSubscribed
        })
    }

    render() {

        let subscribes = [];
        if (this.props.allUsers) {
            this.props.allUsers.forEach(element => {
                if (element.uid == this.props.myID) {
                    subscribes = element.subscribes ? element.subscribes : [];
                }
            });
        }
        
        //var history = null;
        var show_history = [];
        if (this.props.history_chats && this.props.history_chats.length > 0) {
            if (this.props.history_chats[0]['history'] == undefined) {
                this.props.history_chats.sort(function(a,b){
                    return new Date(a.time) - new Date(b.time);
                  });
                show_history = this.props.history_chats;
            } else {
                this.props.history_chats[0]['history'].sort(function(a,b){
                    return new Date(a.time) - new Date(b.time);
                  });
                show_history = this.props.history_chats[0]['history'];
            } 
        }

        if (this.props.displayName != '') {
            return (
                <div>
                <div className="chat-header clearfix">
                    <img src= {this.props.photoURL} alt="avatar" />
                    <div className="chat-about">
                        <div className="chat-with"> <i>Chat with</i> {this.props.displayName}</div>
                        {/* <div className="chat-num-messages">already 1 902 messages</div> */}
                    </div>
                    {
                        subscribes.indexOf(this.props.uid) > -1 ? 
                        <i className="fa fa-star subscribed" onClick = {this.handleSubscribe}/>
                        :
                        <i className="fa fa-star" onClick = {this.handleSubscribe}/>
                    }
                    {/* <i className={this.isActive()} onClick = {this.handleSubscribe}/> */}
                    {/* <i className="fa fa-star" onClick = {this.handleSubscribe}/> */}
                </div>
    
                <div className="chat-history">
                    <ul>
                        {show_history.length > 0 ? show_history.map((one_chat, index) =>{
                            if (one_chat.sender == this.props.myID) {
                                return (
                                    <li key={index+this.props.myID}>
                                        <div className="message-data align-left">
                                            <span className="message-data-name"><i className="fa fa-circle online" /> Me</span>
                                            <span className="message-data-time">{formatDate(one_chat.time)}</span>
                                        </div>
                                        <div className="message my-message">
                                            {one_chat.message.split(" ").map((word) => this.convertWordToImage(word))}
                                        </div>
                                    </li>
                                )
                            }
                            return (
                                <li className="clearfix" key={index}>
                                    <div className="message-data align-right">
                                        <span className="message-data-time">{formatDate(one_chat.time)}</span> &nbsp; &nbsp;
                                        <span className="message-data-name">{this.props.displayName}</span> <i className="fa fa-circle me" />
                                    </div>
                                    <div className="message other-message float-right">
                                        {one_chat.message.split(" ").map((word) => this.convertWordToImage(word))}
                                    </div>
                                </li>
                            )
                        })
                        :
                        <h2>Nothing to show</h2>}
                        
                    </ul>
                </div>
    
                <div className="chat-message clearfix">
                    <textarea name="message-to-send" id="message-to-send" value={this.state.message} placeholder="Type your message" rows={3} onChange = {this.handleMessage} />
                    <i className="fa fa-file-o" /> &nbsp;&nbsp;&nbsp;
                    <i className="fa fa-file-image-o" />
                    <button onClick={this.handleSendMessage}>Send</button>
                </div> 
            </div>
            )
        }
         else {
            return (
                <div className='no-chat'>
                    <h2>Choose someone to Chat!</h2>
                </div>
            )
        }
    }      
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        sendMessage: (chat_id, sender, message) => {
            dispatch(sendMessage(chat_id, sender, message))
        },
        subscribe: (myID, subscribes) => {
            dispatch(subscribe(myID, subscribes))
        }
    }
}

export default compose(
    firestoreConnect((props) => props.chatID ? ['chats/'+props.chatID+'/history']: ['users']),
    connect((state, props) => ({
        myID: state.firebase.auth.uid,
        subscribes: state.auth.subscribes ?  state.auth.subscribes: [],
        allUsers: state.firestore.ordered.users,
      uid: state.chat.uid,
      displayName: state.chat.displayName,
      photoURL: state.chat.photoURL,
      chatID: props.chatID,
      history_chats: state.firestore.ordered.chats? state.firestore.ordered.chats : null,
      link: state.chat
    }), mapDispatchToProps)
   )(MessageBox)