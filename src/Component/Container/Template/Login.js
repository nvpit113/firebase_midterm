import React, {Component} from 'react'
import { connect } from 'react-redux'
import { signIn } from './../../Presentation/Store/Action/Auth'
import {Redirect } from 'react-router-dom'
class SignIn extends Component {
    
    render() {
        
        if (this.props.uid) {
            return <Redirect to='/dasboard'/>
        }
        else
        return (
            <div>
                <h2>Welcome to Firebase Chat</h2>
                <h3>Let's sign in first</h3>
                <button className="btn btn-primary btn-lg fab fa-google" onClick ={()=>this.props.signIn()}>Sign in with Google</button>
            </div>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        uid: state.firebase.auth.uid
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        signIn: () => {
            dispatch(signIn())
        }
    }
}
export default connect(mapStateToProps,mapDispatchToProps) (SignIn)