import firebase from 'firebase/app';
import 'firebase/firestore'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyCS9JjI1yPXSS35gr4akh46lD8qz4O0msI",
    authDomain: "realtimechat-3b1a3.firebaseapp.com",
    databaseURL: "https://realtimechat-3b1a3.firebaseio.com",
    projectId: "realtimechat-3b1a3",
    storageBucket: "realtimechat-3b1a3.appspot.com",
    messagingSenderId: "845030083100"
};

firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots:true });

//const database = firebase.database();

export default firebase;
