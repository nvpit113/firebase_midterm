const authInitialState = {  
    uid: '',
    subscribes: []
};
const authReducer = (state = authInitialState, action) => {
    switch (action.type) {
        case "LOGIN_SUCCESS":
            console.log("Dang Nhap thanh cong");
            return {...state, uid: action.uid, subscribes: action.subscribes};
        case "LOGIN_FAIL":
            console.log("Dang Nhap that bai");
            return state;
        case "LOGOUT_SUCCESS":
            console.log("Dang xuat thanh cong");
            
            return {...state}
        case "LOGOUT_FAIL":
            console.log("Dang xuat that bai");
            return state;

        case "SUBSCRIBE":
            console.log("Thuc hien chuc nang subscribe");
            return {...state, subscribes: action.subscribes};
        default:
            return state;
    }
}

export default authReducer;