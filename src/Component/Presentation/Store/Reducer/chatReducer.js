const chatInitialState = {
    uid: '',
    displayName: '',
    photoURL: '',
    chatID: ''
}
const chatReducer = (state = chatInitialState, action) => {
    switch (action.type) {
        case "START_CHAT":
            return {...state, uid: action.uid, displayName: action.displayName, photoURL: action.photoURL, chatID: action.chatID}
        case "SEND_MESSAGE":
            console.log('Da thuc hien send message');
            
            return state
        default:
            return state
    }
}
export default chatReducer;