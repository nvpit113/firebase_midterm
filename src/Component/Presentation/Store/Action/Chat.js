export const startChat = (uid, display_name, photo_url, chat_id) =>{
    return (dispatch, getState, {getFirebase}) =>{
        dispatch({type:'START_CHAT',uid: uid, displayName: display_name, photoURL: photo_url, chatID: chat_id})
    }
}

export const sendMessage = (chat_id, sender, message) =>{
    return (dispatch, getState, {getFirebase}) =>{
        const firebase = getFirebase();
        const db = firebase.firestore();
        const chats_ref = db.collection("chats"); 
        chats_ref.doc(chat_id).collection('history')
        .add({
            sender: sender,
            message: message,
            time: Date()
        });
        dispatch({type:'SEND_MESSAGE'})
    }
}