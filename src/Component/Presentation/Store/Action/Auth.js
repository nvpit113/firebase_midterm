export const signIn = () =>{
    return (dispatch, getState, {getFirebase}) =>{
        const firebase = getFirebase();
        const db = firebase.firestore();
        const users_ref = db.collection("users"); 
        const provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider)
        .then(result => {
            users_ref.doc(result.user.uid)
            .set({
                uid: result.user.uid,
                displayName: result.user.displayName,
                photoURL: result.user.photoURL,
                lastSignIn: Date(),
                isOnline: true,
            },{merge:true})
            
            let subscribes = result.user.subscribes ? result.user.subscribes : [];
            console.log(subscribes);
            
            dispatch({type:'LOGIN_SUCCESS', uid:result.user.uid, subscribes: subscribes})
        })
        .catch(err =>{
            dispatch({type:'LOGIN_FAIL'})
        })
    }
}

export const signOut = (uid) =>{
    return (dispatch, getState, {getFirebase}) =>{
        const firebase = getFirebase();
        const db = firebase.firestore();
        const users_ref = db.collection("users"); 
        firebase.auth().signOut()
        .then(result => {
            users_ref.doc(uid)
            .set({
                lastSignIn: Date(),
                isOnline: false
            },{merge:true})
            dispatch({type:'LOGOUT_SUCCESS'})
        })
        .catch(err =>{
            dispatch({type:'LOGOUT_FAIL'})
        })
    }
}
export const subscribe = (uid, subscribes) =>{
    return (dispatch, getState, {getFirebase}) =>{
        const firebase = getFirebase();
        const db = firebase.firestore();
        const users_ref = db.collection("users");

        users_ref.doc(uid)
        .set({
            subscribes: subscribes
        },{merge:true})
        dispatch({type:'SUBSCRIBE', subscribes: subscribes})
    }
}