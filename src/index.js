import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker'
import { reduxFirestore, getFirestore } from 'redux-firestore'
import { reactReduxFirebase, getFirebase } from 'react-redux-firebase'
import { createStore, applyMiddleware, compose } from 'redux';
import fbConfig from './Component/Presentation/Firebase/fbConfig'
import rootReducer from './Component/Presentation/Store/Reducer/rootReducer'
import thunk from 'redux-thunk'

//Create Store
var store = createStore(rootReducer,
    compose(
        applyMiddleware(thunk.withExtraArgument({getFirestore, getFirebase})),
        reduxFirestore(fbConfig),
        reactReduxFirebase(fbConfig, {attachAuthIsReady: true})
    ) 
);

// ReactDOM.render(
//     <Provider store={store}>
//         <App />
//     </Provider>
// , document.getElementById('root'));
store.firebaseAuthIsReady.then(() =>{
    ReactDOM.render(
        <Provider store={store}>
            <App />
        </Provider>
    , document.getElementById('root'));
})


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
